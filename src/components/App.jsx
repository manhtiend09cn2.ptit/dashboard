import React , {useState , useEffect} from "react";
import { createTheme, ThemeProvider } from "@mui/material/styles";
import {
	Table,
	TableRow,
	TableHead,
	TableBody,
	TableCell,
	Container,
	TableContainer,
	Button,
	Grid,
} from "@mui/material";
import { useQuery, gql } from "@apollo/client";
import Banner  from "../subComponents/Banner"
import MoviesComponent from "../subComponents/MoviesComponent";

const GET_TRANSACTIONS = gql`
	query Transactions {
		transactions {
			amount
			bankName
			id
			type
		}
	}
`;

const GET_USER = gql`
	query User($userId: String) {
		user(id: $userId) {
			accountNumber
			balance
			displayName
			id
		}
	}
`;

const TABLE_HEAD = [
	{ id: "id", label: "Mã Phim", alignRight: false },
	{ id: "bankName", label: "Tên phim", alignRight: false },
	{ id: "amount", label: "Giá vé", alignRight: false },
	{ id: "type", label: "Thể loại", alignRight: false },
];

const defaultTheme = createTheme();

 const Dashboard = () => {

	 console.log('dataToken')
	// const { loading, error, data } = useQuery(GET_TRANSACTIONS, {});

	// const { data: userData } = useQuery(GET_USER, {
	// 	variables: { userId: JSON.parse(localStorage.getItem("user"))?.id },
	// });

	// if (loading) return null;
	// if (error) return `Error! ${error}`;


	useEffect( () => {
		const dataToken = localStorage.getItem("access_token", JSON.stringify(''));
		if(dataToken === '""')
		{
			window.location = "/";
			return;
		}

		console.log('dataToken : ' , dataToken)
		console.log('dataToken : ' , dataToken === '""')

	}, [])

	 const movies  = [{
			movie_type : 'Kinh Dị',
			movie_name : 'Chiến tranh giữa các vì sao',
			movie_description : 'Chiến tranh giữa các vì sao',
			movie_image : '',
			movie_price : '1000000'
		}];

	let user;

	// if (userData) {
	// 	user = userData.user;
	// }

	return (
		<ThemeProvider theme={defaultTheme}>
			<Banner/>
			<Container component="main" maxWidth="xl">
				<Grid container spacing={2}>
					<Grid item xs={8}>
						<h1>Phim đang chiếu</h1>
					</Grid>
					<Grid item xs={4} textAlign={"right"}>
					</Grid>
				</Grid>
                <MoviesComponent/>
			</Container>
		</ThemeProvider>
	);
}


export default Dashboard;
