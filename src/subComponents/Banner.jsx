import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import "react-responsive-carousel/lib/styles/carousel.min.css"; // requires a loader
import { Carousel } from 'react-responsive-carousel';
import {
    Grid,
    Container,
    Typography,
    AppBar,
    IconButton,
    Box,
    Avatar,
    Toolbar,
    Button,
    CardMedia
} from "@mui/material";
export default class Banner extends Component {
    render() {
        return (
            <Carousel timer={50} autoPlay={true} infiniteLoop={true} >
                <div>
                    <CardMedia
                        component="img"
                        height="500"
                        src="https://marketplace.canva.com/EAFTl0ixW_k/1/0/1131w/canva-black-white-minimal-alone-movie-poster-YZ-0GJ13Nc8.jpg"
                        alt="Paella dish"
                    />
                </div>
                <div>
                    <CardMedia
                        component="img"
                        height="500"
                        src="https://visme.co/blog/wp-content/uploads/2017/12/MINIMALIST-MOVIE-POSTERS-Chungkon-The-Arrival.png"
                        alt="Paella dish"
                    />
                </div>
                <div>
                    <CardMedia
                        component="img"
                        height="500"
                        src="https://www.washingtonpost.com/graphics/2019/entertainment/oscar-nominees-movie-poster-design/img/black-panther-web.jpg"
                        alt="Paella dish"
                    />
                </div>
            </Carousel>
        );
    }
};
